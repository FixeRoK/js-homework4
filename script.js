// // отримання двох чисел від користувача
// const number1 = parseFloat(prompt("Введіть перше число:"));
// const number2 = parseFloat(prompt("Введіть друге число:"));
//
// // отримання операції від користувача
// const operator = prompt("Введіть операцію (+, -, *, /):");
//
// // функція для виконання математичної операції
// function calculate(num1, num2, op) {
//     let result;
//     switch (op) {
//         case "+":
//             result = num1 + num2;
//             break;
//         case "-":
//             result = num1 - num2;
//             break;
//         case "*":
//             result = num1 * num2;
//             break;
//         case "/":
//             result = num1 / num2;
//             break;
//         default:
//             result = "Некоректна операція";
//     }
//     return result;
// }
//
// // виклик функції та вивід результату в консоль
// const result = calculate(number1, number2, operator);
// console.log(`Результат: ${result}`);





let number1 = parseFloat(prompt("Введіть перше число:"));
let number2 = parseFloat(prompt("Введіть друге число:"));

while (isNaN(number1) || isNaN(number2)) {
    alert("Введене значення не є числом. Спробуйте ще раз.");
    number1 = parseFloat(prompt(`Введіть перше число: `));
    number2 = parseFloat(prompt(`Введіть друге число: `));
}

const operator = prompt("Введіть операцію (+, -, *, /):");


function calculate(num1, num2, op) {
    let result;
    switch (op) {
        case "+":
            result = num1 + num2;
            break;
        case "-":
            result = num1 - num2;
            break;
        case "*":
            result = num1 * num2;
            break;
        case "/":
            result = num1 / num2;
            break;
        default:
            result = "Некоректна операція";
    }
    return result;
}

const result = calculate(number1, number2, operator);
console.log(`Результат: ${result}`);
